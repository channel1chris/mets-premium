<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once "report/includes/classes/Database.php";
include_once "report/includes/classes/Browser.php";
include_once "report/includes/models/Tracking.php";
include_once "report/includes/models/Accounts.php";

if(isset($_GET['type'])) {
    $type = isset($_GET['type']) ? $_GET['type'] : "";
    $account_id = isset($_GET['account_id']) ? $_GET['account_id'] : "";
    $urls = array(

        "Renew" => "https://oss.ticketmaster.com/html/home.htmI?l=EN&team=azdbacks&partnerId=131DCZB474Q1-190",
        "Invoice" => "invoice.php",
        "Opt-Out" => "opt-out.php",
        "RAVEFoundationPrograms" => "http://www.ravefoundation.org/programs",
        "RSVP" => "http://www.soundersfc.com/stmcelebration",
        "TicketPolicies" => "",
        "S2" => "https://www.soundersfc.com/post/2017/05/08/seattle-sounders-fc-and-tacoma-rainiers-agree-memorandum-understanding-build-soccer",
        "TicketTransferPolicies" => "",
        "RaveFoundation" => ""
    );

    $pdo = Database::getInstance()->getConnection();
    $tracking = new Tracking($pdo);
    $account = new Accounts($pdo);

    $tracking->updateTrackingParams($type);

    if($type == "Renew" && $account_id != "")
        $account->saveRenewDate($account_id);

    header("Location: ".$urls[$type]);
    exit;
}

?>