<?php

include_once "includes/classes/Database.php";
include_once "includes/models/Accounts.php";

$pdo = Database::getInstance()->getConnection();
$accounts = new Accounts($pdo);

$entries = array();

if(isset($_GET['purl_type']) && $_GET['purl_type'] == "no_hits") {
    if(isset($_GET['sort'])) {
        $sort = $_GET['sort'];
        $entries = $accounts->getPurls($sort,"purl_no_hits");
    } else {
        $entries = $accounts->getPurls("default","purl_no_hits");
    }
} else {
    if(isset($_GET['sort'])) {
        $sort = $_GET['sort'];
        $entries = $accounts->getPurls($sort,"purl_hits");
    } else {
        $entries = $accounts->getPurls("default","purl_hits");
    }
} ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="icon" href="../images/logo.png" sizes="32x32">

    <title>Atlanta Hawks</title>

    <link href="assets/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css">
    <link href="assets/css/menu.css" rel="stylesheet" type="text/css">
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css">

    <script src="assets/js/modernizr.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>


<body class="fixed-left">
<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <a href="#" class="logo"><img src="assets/images/channel1-logo.png" alt="Channel 1 Logo"/> </a>
            </div>
        </div>
        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="">
                    <div class="pull-left">
                        <button class="button-menu-mobile open-left">
                            <i class="fa fa-bars"></i>
                        </button>
                        <span class="clearfix"></span>
                    </div>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <div class="user-details">
                <div class="pull-left">
                    <img src="../images/logo.png" alt="" class="thumb-md img-circle">
                </div>
                <div class="user-info">
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Hawks <span class="caret"></span></a>
                    </div>

                    <p class="text-muted m-0">Admin</p>
                </div>
            </div>
            <!--- Divider -->
            <div id="sidebar-menu">
                <ul>
                    <li><a href="index.php" class="waves-effect waves-light"><i class="md md-home"></i><span> Dashboard </span></a></li>
                    <li><a href="purls.php" class="waves-effect waves-light active"><i class="md md-people"></i><span> PURLS </span></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb pull-right">
                            <li><a href="#"></a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php if(isset($_GET['purl_type']) && $_GET['purl_type'] == "no_hits"): ?>
                                    <h3 class="panel-title">UNIQUE PURLS NO HITS</h3>
                                <?php else: ?>
                                    <h3 class="panel-title">UNIQUE PURLS HITS</h3>
                                <?php endif; ?>
                            </div>
                            <div class="panel-body">
                                <?php if(isset($_GET['purl_type']) && $_GET['purl_type'] == "no_hits"): ?>
                                    <a class="btn btn-default" href="purls.php?purl_type=hits">Display Unique Purl Hits</a>
                                    <a class="btn btn-primary" href="export_purl.php?purl_type=no_hits">Export Unique Purl No Hits</a>
                                <?php else: ?>
                                    <a class="btn btn-default" href="purls.php?purl_type=no_hits">Display Unique Purl No Hits</a>
                                    <a class="btn btn-primary" href="export_purl.php?purl_type=hits">Export Unique Purl Hits</a>
                                <?php endif; ?>
                                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                        <?php if(isset($_GET['purl_type']) && $_GET['purl_type'] == "no_hits"): ?>
                                            <th><a href="?purl_type=no_hits&sort=Account">Account ID</a></th>
                                            <th><a href="?purl_type=no_hits&sort=FullName">Full Name</a></th>
                                            <th><a href="?purl_type=no_hits&sort=purl">Purl</a></th>
                                            <th><a href="?purl_type=no_hits&sort=Rep">Rep</a></th>
                                        <?php else: ?>
                                            <th><a href="?purl_type=no_hits&sort=lasttime">Last Visited</a></th>
                                            <th><a href="?purl_type=purl_hits&sort=Account">Account ID</a></th>
                                            <th><a href="?purl_type=purl_hits&sort=FullName">Full Name</a></th>
                                            <th><a href="?purl_type=purl_hits&sort=hits">Hits</a></th>
                                            <th><a href="?purl_type=purl_hits&sort=purl">Purl</a></th>
                                            <th><a href="?purl_type=purl_hits&sort=Rep">Rep</a></th>
                                        <?php endif; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for($i = 0, $len = count($entries);$i < $len;$i++): ?>
                                            <tr>
                                                <?php if(isset($_GET['purl_type']) && $_GET['purl_type'] == "no_hits"): ?>
                                                <td><?php echo $entries[$i]['Account']; ?></td>
                                                <td><?php echo $entries[$i]['FullName']; ?></td>
                                                <td><?php echo $entries[$i]['purl']; ?></td>
                                                <td><?php echo $entries[$i]['Rep']; ?></td>
                                                <?php else: ?>
                                                <td><?php echo $entries[$i]['lasttime']; ?></td>
                                                <td><?php echo $entries[$i]['Account']; ?></td>
                                                <td><?php echo $entries[$i]['FullName']; ?></td>
                                                <td><?php echo $entries[$i]['hits']; ?></td>
                                                <td><?php echo $entries[$i]['purl']; ?></td>
                                                <td><?php echo $entries[$i]['Rep']; ?></td>
                                            <?php endif; ?>
                                            </tr>
                                        <?php endfor; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Row -->
            </div> <!-- container -->

        </div> <!-- content -->
    </div>
</div>
<!-- END wrapper -->
<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>

<script src="assets/js/jquery.app.js"></script>

<!-- moment js  -->
<script src="assets/plugins/moment/moment.js"></script>

<!-- counters  -->
<script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
<script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

<!-- flot Chart -->
<script src="assets/plugins/flot-chart/jquery.flot.js"></script>
<script src="assets/plugins/flot-chart/jquery.flot.time.js"></script>
<script src="assets/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="assets/plugins/flot-chart/jquery.flot.resize.js"></script>
<script src="assets/plugins/flot-chart/jquery.flot.pie.js"></script>
<script src="assets/plugins/flot-chart/jquery.flot.selection.js"></script>
<script src="assets/plugins/flot-chart/jquery.flot.stack.js"></script>
<script src="assets/plugins/flot-chart/jquery.flot.crosshair.js"></script>

<!-- todos app  -->
<script src="assets/pages/jquery.todo.js"></script>

<!-- chat app  -->
<script src="assets/pages/jquery.chat.js"></script>

<!-- dashboard  -->
<script src="assets/pages/jquery.dashboard.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.counter').counterUp({
            delay: 100,
            time: 1200
        });
    });
</script>


</body>
</html>
