<?php

class Accounts
{
    private $_db;

    public function __construct($db) {
        $this->_db = $db;
    }

    public function getStatsData() {
        $q = "SELECT (SELECT hits FROM hawks2018_tracking) as total_hits,
         (SELECT count(DISTINCT Account) FROM hawks2018_accounts) as total_unique_purls,
         (SELECT count(DISTINCT Account) FROM hawks2018_accounts where hits > 0) as total_purls_hits,
         (SELECT count(DISTINCT Account) FROM hawks2018_accounts where hits <= 0) as total_purls_no_hits,
         (SELECT Invoice FROM hawks2018_tracking) as total_Invoice_hits,
         (SELECT Renew FROM hawks2018_tracking) as total_Renew_hits,
         (SELECT Print FROM hawks2018_tracking) as total_print_hits,
         (SELECT MyHawksAccount FROM hawks2018_tracking) as total_MyHawksAccount_hits,
         (SELECT CEOLetter FROM hawks2018_tracking) as total_CEOLetter_hits,
         (SELECT HawksApp FROM hawks2018_tracking) as total_HawksApp_hits,
         (SELECT Terms FROM hawks2018_tracking) as total_Terms_hits,
         (SELECT MobileTickets FROM hawks2018_tracking) as total_MobileTickets_hits,
         (SELECT InvoiceTerms FROM hawks2018_tracking) as total_InvoiceTerms_hits,
         (SELECT Twitter FROM hawks2018_tracking) as total_twitter_hits,
         (SELECT Facebook FROM hawks2018_tracking) as total_facebook_hits,
         (SELECT Print FROM hawks2018_tracking) as total_print_hits,
         (SELECT Instagram FROM hawks2018_tracking) as total_instagram_hits,
         (SELECT Snapchat FROM hawks2018_tracking) as total_snapchat_hits,
         (SELECT count(DISTINCT Account) FROM hawks2018_accounts WHERE renew_hits > 0) as total_renew_hits";

        $result = $this->_db->query($q, PDO::FETCH_ASSOC);
        return $result->fetch();
    }

    public function saveHit($Account) {
        $q = "UPDATE hawks2018_accounts SET hits = hits + 1, lasttime = now() WHERE Account = :Account";
        $stmt = $this->_db->prepare($q);

        $stmt->execute(array(
            ":Account" =>  $Account
        ));
    }

    public function saveRenewDate($Account) {
        $q = "UPDATE hawks2018_accounts SET renew_hits = renew_hits + 1, renew_date = now() WHERE Account = :Account";
        $stmt = $this->_db->prepare($q);

        $stmt->execute(array(
            ":Account" =>  $Account
        ));
    }

    public function getPurls($sort,$type) {
        $q = "";

        if($type == "purl_hits") {
            if($sort == "default"){
                $q = "SELECT DISTINCT Account, Rep, lasttime, FullName, hits, purl FROM hawks2018_accounts WHERE hits > 0 AND FullName != 'Rick Amos'";
            } else {
                $q = "SELECT DISTINCT Account, Rep, lasttime, FullName, hits, purl FROM hawks2018_accounts WHERE hits > 0 AND FullName != 'Rick Amos' ORDER BY $sort";
            }
        } else if($type == "purl_no_hits") {
            if($sort == "default"){
                $q = "SELECT DISTINCT Account, Rep, lasttime, FullName, hits, purl FROM hawks2018_accounts WHERE hits <= 0 AND FullName != 'Rick Amos'";
            } else {
                $q = "SELECT DISTINCT Account, Rep, lasttime, FullName, hits, purl FROM hawks2018_accounts WHERE hits <= 0 AND FullName != 'Rick Amos' ORDER BY $sort";
            }
        }

        $result = $this->_db->query($q, PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
}