<?php

class Tracking {

    private $_db;
    private $_params;

    public function __construct($db) {
        $this->_db = $db;
        $this->_params = array("hits", "Invoice", "Renew", "Print", "Facebook", "Twitter", "Instagram", "Snapchat", "MyHawksAccount", "CEOLetter", "HawksApp", "Terms", "MobileTickets", "InvoiceTerms");
    }

    public function updateTrackingParams($type) {
        if(in_array($type, $this->_params)) {
            $q = "UPDATE hawks2018_tracking SET ".$type." = ".$type."+ 1";
            $stmtT = $this->_db->prepare($q);
            $stmtT->execute();
        }
    }

    public function saveBrowsingDevice() {
        $browser = new Browser();
        $result = $browser->getBrowser();
        $qS = "";

        if(in_array($result, array(Browser::BROWSER_FIREFOX, Browser::BROWSER_SAFARI, Browser::BROWSER_CHROME, Browser::BROWSER_IE))) {
            switch($result){
                case Browser::BROWSER_FIREFOX:
                    $qS .= "SET firefox = firefox + 1";
                    break;
                case Browser::BROWSER_SAFARI:
                    $qS .= "SET safari = safari + 1";
                    break;
                case Browser::BROWSER_CHROME:
                    $qS .= "SET chrome = chrome + 1";
                    break;
                case Browser::BROWSER_IE:
                    $qS .= "SET ie = ie + 1";
                    break;
            }

            $q = "UPDATE hawks2018_tracking ".$qS;
            $stmt = $this->_db->prepare($q);
            $stmt->execute();
        }
    }

    public function getBrowsingStats() {
        $q = "SELECT chrome, firefox, safari, ie FROM hawks2018_tracking";

        $result = $this->_db->query($q);

        if($result->rowCount() > 0) {
            return $result->fetch(PDO::FETCH_ASSOC);
        }

        return array();
    }

    public function getSessions() {
        $q = "SELECT DATE_FORMAT(renew_date, '%m/%d/%Y') as day_visited, count(id) as count FROM hawks2018_accounts WHERE renew_date >= ( CURDATE() - INTERVAL 7 DAY ) GROUP BY day_visited";

        $result = $this->_db->query($q);

        if($result->rowCount() > 0) {
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }

        return array();
    }
}