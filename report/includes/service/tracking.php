<?php

include_once "../models/Tracking.php";
include_once "../classes/Database.php";

if(isset($_GET['browser'])) {
    $pdo = Database::getInstance()->getConnection();
    $tracking = new Tracking($pdo);

    echo json_encode($tracking->getBrowsingStats());
    exit;
}

if(isset($_GET['visits'])) {
    $pdo = Database::getInstance()->getConnection();
    $tracking = new Tracking($pdo);

    echo json_encode($tracking->getSessions());
    exit;
}