<?php
include_once "classes/Database.php";

$pdo = Database::getInstance()->getConnection();

function clean($string) {
    $string = str_replace(' ', '.', $string);
    $string = preg_replace('/[^A-Za-z0-9\-.]/', '', $string); // Removes special chars.
    return str_replace('..', '.', str_replace("..", ".", $string));
}

$q = "SELECT id, account_id, full_name FROM whitesox2017_accounts";

foreach($pdo->query($q, PDO::FETCH_ASSOC) as $row) {
    $id = isset($row['id']) ? $row['id'] : "";
    $name = isset($row['full_name']) ? trim(strtolower(str_replace(" ", ".", str_replace("'", "", $row['full_name'])))) : "";
    $account = isset($row['account_id']) ? $row['account_id'] : "";
    $url = "http://whitesox.brochure-mlb.com/2017/";
    $p = "";

    if($name != "") {
        $p .= $name.".";

        if($last != "")
            $p .= $last.".";

        $p .= $account;
    } else {
        if($last != "")
            $p .= $last.".";

        $p .= $account;
    }

    $p = md5(clean($p));
    $p = substr($p, 0, 8);

    $q = "UPDATE whitesox2017_accounts SET purl = :purl, purl_url = :purl_url WHERE id = :id";
    $stmt = $pdo->prepare($q);
    $stmt->execute(array(
        ":purl" => $p,
        ":purl_url" => $url.$p,
        ":id" => $row['id']
    ));

    if($stmt->rowCount()) {
        echo "PURL CREATED " . $row['account_id']." - ".$p."<br />";
    }
}