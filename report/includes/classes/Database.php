<?php

/**
 * Singleton Pattern Database
 */
class Database
{
    private $_connection;
    private static $_instance;

    private $dbhost = "localhost";
    private $dbname = "channel1media_com_-_2018";//white_sox
    private $dbuser = "adminC1MS";//root
    private $dbpwd = "zQAFS4hS5DBSxUVs";//webandsoftware

    private function __construct() {
        try {
            $this->_connection = new PDO("mysql:host=".$this->dbhost.";dbname=".$this->dbname, $this->dbuser, $this->dbpwd);
            $this->_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            die("Can not connect to mysql");
        }
    }

    private function __clone() {}

    public static function getInstance() {
        if(!self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getConnection() {
        return $this->_connection;
    }
}