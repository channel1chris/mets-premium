<?php
include_once "includes/classes/Database.php";
include_once "includes/models/Accounts.php";

$pdo = Database::getInstance()->getConnection();
$account = new Accounts($pdo);
$entries = array();

if(isset($_GET['purl_type']) && $_GET['purl_type'] == "no_hits") {
    $entries = $account->getPurls("default","purl_no_hits");

    $rawFile = "exports/purls_no_hits.csv";
    $file = @fopen($rawFile, 'w+');

    fwrite($file, "Account, FullName, Purl\r\n");

    foreach($entries as $values) {
        @fwrite($file, $values["Account"].",".$values["FullName"].",".$values['purl']."\r\n");
    }
}
else {
    $entries = $account->getPurls("default","purl_hits");

    $rawFile = "exports/purls_hits.csv";
    $file = @fopen($rawFile, 'w+');

    fwrite($file, "Last Visited, Account, FullName, Total Hits, Purl\r\n");

    foreach($entries as $values) {
        @fwrite($file, $values["lasttime"].",".$values["Account"].",".$values["FullName"].",".$values['hits'].",".$values['purl']."\r\n");
    }
}


fclose($file);

if (file_exists($rawFile)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($rawFile).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($rawFile));
    readfile($rawFile);
    exit;
}

?>
