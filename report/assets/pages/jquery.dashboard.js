/**
* Theme: Moltran Admin Template
* Author: Coderthemes
* Module/App: Dashboard
*/


!function($) {
    "use strict";

    var Dashboard = function() {
        this.$body = $("body");
        this.$realData = [];
    };

    //creates plot graph
    Dashboard.prototype.createPlotGraph = function(selector, data1, labels, colors, borderColor, bgColor, ticks) {
        //shows tooltip
        function showTooltip(x, y, contents) {
            $('<div id="tooltip" class="tooltipflot">' + contents + '</div>').css( {
                position: 'absolute',
                top: y + 5,
                left: x + 5
            }).appendTo("body").fadeIn(200);
        }

        var currDate = new Date();
        var min15Day = new Date();

        min15Day.setDate(min15Day.getDate() - 2);

        $.plot($(selector),
            [ { data: data1,
                label: labels[0],
                color: colors[0]
            }
            ],
            {
                series: {
                    lines: {
                        show: true,
                        //fill: true,
                        lineWidth: 1,
                        fillColor: {
                            colors: [ { opacity: 0.2 },
                                { opacity: 0.9 }
                            ]
                        }
                    },
                    points: {
                        show: true
                    },
                    shadowSize: 0
                },
                legend: {
                    position: 'nw'
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    borderColor: borderColor,
                    borderWidth: 0,
                    labelMargin: 10,
                    backgroundColor: bgColor
                },
                yaxis: {
                    min: 0,
                    color: 'rgba(0,0,0,0)'
                },
                xaxis: {
                    color: 'rgba(0,0,0,0)',
                    mode: "time", timeformat: "%m/%d/%y",
                    ticks: ticks
                },
                tooltip: true,
                tooltipOpts: {
                    content: '%s: Value of %x is %y',
                    shifts: {
                        x: -60,
                        y: 25
                    },
                    defaultTheme: false
                }
            });
    };
    //end plot graph

    //creates Pie Chart
    Dashboard.prototype.createPieGraph = function(selector, labels, datas, colors) {
        var data = [{
            label: labels[0],
            data: datas[0]
        }, {
            label: labels[1],
            data: datas[1]
        }, {
            label: labels[2],
            data: datas[2]
        },{
            label: labels[3],
            data: datas[3]
        }];
        var options = {
            series: {
                pie: {
                    show: true
                }
            },
            legend : {
              show : false
            },
            grid : {
              hoverable : true,
              clickable : true
            },
            colors : colors,
            tooltip : true,
            tooltipOpts : {
              content : "%s, %p.0%"
            }
        };

        $.plot($(selector), data, options);
    };

    

    //initializing various charts and components
    Dashboard.prototype.init = function() {
        var that = this;

        $.get("includes/service/tracking.php?visits=1", function( data ) {
            var visitsObj = JSON.parse(data);
            var visits = [],
                ticks = [];

            for(var i = 0;i < visitsObj.length;i++) {
                var date = new Date(visitsObj[i].day_visited);
                date.setDate(date.getDate() + 1);
                var time = date.getTime();


                visits.push([time, parseInt(visitsObj[i].count)]);
                ticks.push(time);
            }

            var plabels = ["Clicks"];
            var pcolors = ['#2b9ac9'];
            var borderColor = '#fff';
            var bgColor = '#fff';
            that.createPlotGraph("#website-stats", visits, plabels, pcolors, borderColor, bgColor, ticks);
        });

        $.get("includes/service/tracking.php?browser=1", function( data ) {
            var browsers = JSON.parse(data);

            if(typeof browsers != "undefined" && browsers != null) {
                var pielabels = ["Chrome", "Safari", "IE", "Firefox"];
                var datas = [browsers['chrome'],browsers['safari'], browsers['ie'], browsers['firefox']];
                var colors = ["rgba(43, 64, 73, 1)", "rgba(43, 154, 201, 1)", "rgba(88, 201, 199, 1)", "rgba(88, 201, 199, 1)"];
                that.createPieGraph("#pie-chart #pie-chart-container", pielabels , datas, colors);
            }
        });
    };

    //init Dashboard
    $.Dashboard = new Dashboard; $.Dashboard.Constructor = Dashboard
    
}(window.jQuery),

//initializing Dashboard
function($) {
    "use strict";
    $.Dashboard.init()
}(window.jQuery);


