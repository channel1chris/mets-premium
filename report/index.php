<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    include_once "includes/classes/Database.php";
    include_once "includes/classes/Browser.php";
    include_once "includes/models/Accounts.php";

    $pdo = Database::getInstance()->getConnection();
    $account = new Accounts($pdo);

    $stats = $account->getStatsData();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Coderthemes">

        <link rel="icon" href="../images/logo.png" sizes="32x32">

        <title>Hawks 2018 Report</title>

        <link href="assets/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css">
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css">
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css">

        <script src="assets/js/modernizr.min.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>


    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
        
            <!-- Top Bar Start -->
            <div class="topbar">
                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <a href="#" class="logo">
                            <div class="logo-bg"></div>
                        </a>
                    </div>
                </div>
                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div class="user-details">
                        <div class="pull-left">
                            <img src="../images/logo.png" alt="" class="thumb-md img-circle">
                        </div>
                        <div class="user-info">
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Hawks</a>
                            </div>

                            <p class="text-muted m-0">Admin</p>
                        </div>
                    </div>
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>
                            <li><a href="index.php" class="waves-effect waves-light active"><i class="md md-home"></i><span> Dashboard </span></a></li>
                            <li><a href="purls.php" class="waves-effect waves-light"><i class="md md-people"></i><span> PURLS </span></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End --> 



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <ol class="breadcrumb pull-right">
                                    <li><a href="#"></a></li>
                                    <li class="active">Dashboard</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-lg-12">
                                <div class="portlet"><!-- /portlet heading -->
                                    <div class="portlet-heading">
                                        <h3 class="portlet-title text-dark text-uppercase">
                                            Browser Stats
                                        </h3>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="portlet2" class="panel-collapse collapse in">
                                        <div class="portlet-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="pie-chart">
                                                        <div id="pie-chart-container" class="flot-chart" style="height: 320px">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- /Portlet -->
                            </div> <!-- end col -->
                        </div> <!-- End row -->

                        <!-- Start Widget -->
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_hits']) ? $stats['total_hits'] : 0; ?></span>
                                        Total Hits To Site
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_unique_purls']) ? $stats['total_unique_purls'] : 0; ?></span>
                                        Total Unique PURLS
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_purls_hits']) ? $stats['total_purls_hits'] : 0; ?></span>
                                        Total Unique Purl Hits
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_purls_no_hits']) ? $stats['total_purls_no_hits'] : 0; ?></span>
                                        Total Unique Purl No Hits
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_Renew_hits']) ? $stats['total_Renew_hits'] : 0; ?></span>
                                        Total Hits to Renew
                                    </div>
                                </div>
                            </div>

                             <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_Invoice_hits']) ? $stats['total_Invoice_hits'] : 0; ?></span>
                                        Total Hits to Invoice
                                    </div>
                                </div>
                            </div>

                             <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_print_hits']) ? $stats['total_print_hits'] : 0; ?></span>
                                        Total Hits to Print
                                    </div>
                                </div>
                            </div>

                             <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_MyHawksAccount_hits']) ? $stats['total_MyHawksAccount_hits'] : 0; ?></span>
                                        Total Hits to My Hawks Account
                                    </div>
                                </div>
                            </div>

                             <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_CEOLetter_hits']) ? $stats['total_CEOLetter_hits'] : 0; ?></span>
                                        Total Hits to CEO Letter
                                    </div>
                                </div>
                            </div>

                             <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_HawksApp_hits']) ? $stats['total_HawksApp_hits'] : 0; ?></span>
                                        Total Hits to Hawks App
                                    </div>
                                </div>
                            </div>

                             <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_Terms_hits']) ? $stats['total_Terms_hits'] : 0; ?></span>
                                        Total Hits to Terms
                                    </div>
                                </div>
                            </div>

                             <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_MobileTickets_hits']) ? $stats['total_MobileTickets_hits'] : 0; ?></span>
                                        Total Hits to Mobile Tickets
                                    </div>
                                </div>
                            </div>

                             <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_InvoiceTerms_hits']) ? $stats['total_InvoiceTerms_hits'] : 0; ?></span>
                                        Total Hits to Invoice Terms
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_facebook_hits']) ? $stats['total_facebook_hits'] : 0; ?></span>
                                        Total Hits to Facebook
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_twitter_hits']) ? $stats['total_twitter_hits'] : 0; ?></span>
                                        Total Hits to Twitter
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_instagram_hits']) ? $stats['total_instagram_hits'] : 0; ?></span>
                                        Total Hits to Instagram
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-lg-4">
                                <div class="mini-stat clearfix bx-shadow">
                                    <span class="mini-stat-icon users-bg"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right">
                                        <span class="counter"><?php echo isset($stats['total_snapchat_hits']) ? $stats['total_snapchat_hits'] : 0; ?></span>
                                        Total Hits to Snapchat
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- End row-->
                    </div> <!-- container -->
                               
                </div> <!-- content -->
            </div>
        </div>
        <!-- END wrapper -->

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <script src="assets/js/jquery.app.js"></script>

        <!-- moment js  -->
        <script src="assets/plugins/moment/moment.js"></script>

        <!-- counters  -->
        <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
        <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

        <!-- flot Chart -->
        <script src="assets/plugins/flot-chart/jquery.flot.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.time.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.resize.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.pie.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.selection.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.stack.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.crosshair.js"></script>

        <!-- todos app  -->
        <script src="assets/pages/jquery.todo.js"></script>
        
        <!-- chat app  -->
        <script src="assets/pages/jquery.chat.js"></script>
        
        <!-- dashboard  -->
        <script src="assets/pages/jquery.dashboard.js"></script>

        <!-- Modal-Effect -->
        <script src="assets/plugins/modal-effect/js/classie.js"></script>
        <script src="assets/plugins/modal-effect/js/modalEffects.js"></script>
        
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 100,
                    time: 1200
                });
            });
        </script>

    
    </body>
</html>