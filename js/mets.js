$(function() {
    initMain();
    
});

var offsetScroll = 90;

function initMain() {
    $(".navbar-nav a, .arrow-down").bind("click", onNavClick);
    $(".accordion").accordion({ heightStyle: "content", collapsible: true });
    $(".subsection-list a").bind("click", toggleSubsections);
    $(".top-menu .benefits-info").bind("click", toggleGalleryInfo);
    $(".top-menu .show-gallery").bind("click", closeGalleryInfo);
    $(".accordion .close-btn").on("click", closeAccordion);
    $(".video-btn").on('click', showFullVideo);
    $(window).on("load", initMainWindowOnLoad);
    
    $("#porsche-suites .subsection-list a").first().click();
    $("#first-data-club .subsection-list a").first().click();
    $("#field-level-seating .subsection-list a").first().click();
    $(".top-menu .benefits-info").click();

    AOS.init();

    // Show and hide drop downs for subnavs
    function showMenu() {
        $(this).removeClass("drop-collapsed");
        $(this).addClass("open");
    }

    function hideMenu() {
        $(this).removeClass("open");
        var $dropdown = $(".dropdown");

        $dropdown.each(function() {
            $(this).addClass("drop-collapsed");
        });
    }


    var $dropdown = $(".dropdown");

    $dropdown.each(function() {
        var $this = $(this);

        var $dropmenu = $this.find(".dropdown-menu");
        $dropmenu.css("height", $dropmenu.outerHeight());
        $this.addClass("drop-collapsed");
    });


    // dropdown menu hover intent
    var hovsettings = {
        timeout: 0,
        interval: 0,
        over: showMenu,
        out: hideMenu
    };

    $(".dropdown").hoverIntent(hovsettings);

    if ($("#porsche-suites").length > 0) {
        var waypoint = new Waypoint({
            element: document.getElementById('porsche-suites'),
            handler: function(direction) {
                if (!$("body").hasClass('linking')) {
                    $('#topnav .navbar-nav a, #mobile-nav a').removeClass('active');
                    $('#topnav .navbar-nav a[data-id="1"], #mobile-nav a[data-id="1"]').addClass('active');
                }
            }
        })
    }

    if ($("#first-data-club").length > 0) {
        var waypoint = new Waypoint({
            element: document.getElementById('first-data-club'),
            handler: function(direction) {
                if (!$("body").hasClass('linking')) {
                    $('#topnav .navbar-nav a, #mobile-nav a').removeClass('active');
                    $('#topnav .navbar-nav a[data-id="2"], #mobile-nav a[data-id="2"]').addClass('active');
                }
            }
        })
    }

    if ($("#hyundai-club").length > 0) {
        var waypoint = new Waypoint({
            element: document.getElementById('hyundai-club'),
            handler: function(direction) {
                if (!$("body").hasClass('linking')) {
                    $('#topnav .navbar-nav a, #mobile-nav a').removeClass('active');
                    $('#topnav .navbar-nav a[data-id="3"], #mobile-nav a[data-id="3"]').addClass('active');
                }
            }
        })
    }

    if ($("#delta-club").length > 0) {
        var waypoint = new Waypoint({
            element: document.getElementById('delta-club'),
            handler: function(direction) {
                if (!$("body").hasClass('linking')) {
                    $('#topnav .navbar-nav a, #mobile-nav a').removeClass('active');
                    $('#topnav .navbar-nav a[data-id="4"], #mobile-nav a[data-id="4"]').addClass('active');
                }
            }
        })
    }

    if ($("#field-level-seating").length > 0) {
        var waypoint = new Waypoint({
            element: document.getElementById('field-level-seating'),
            handler: function(direction) {
                if (!$("body").hasClass('linking')) {
                    $('#topnav .navbar-nav a, #mobile-nav a').removeClass('active');
                    $('#topnav .navbar-nav a[data-id="5"], #mobile-nav a[data-id="5"]').addClass('active');
                }
            }
        })
    }

}


function onNavClick() {
    if (!$(this).hasClass('active') && !$(this).hasClass('dropdown-toggle')) {
        var link = $(this).attr("data-id");
        $(".navbar-nav a").removeClass('active');
        $(this).addClass('active');
        $(this).parent().siblings("li").removeClass('active');
        if ($(this).parents().eq(1).hasClass('dropdown-menu')) {
            $(this).parents().eq(1).prev().addClass('active');
        }
        if ($(this).hasClass('has-sub')){
            $(this).parent().siblings("li").removeClass('active');
            $(this).parent().addClass('active');
            return false;
        }
        var goTo = "#" + $('section[data-id="' + link + '"]').attr("id");
        $("body").addClass('linking');
        anchorScroll($(this), $(goTo), 400);
    }
    if ($("#mobile-nav").hasClass('collapse in')) {
        $('.navbar-toggle').trigger('click');
    }
    return false;
}

function toggleBenefits() {
    var benefit = $(this).attr("data-id");
    if (!$(this).hasClass('active')) {
        $(this).addClass('active');
        $(this).siblings("a").removeClass('active');

        if (benefit == "diamond" || benefit == "platinum" || benefit == "gold") {
            $(".benefits div.special strong").text("Tier 1");
        } else {
            $(".benefits div.special strong").text("Tier 2");
        }

        if (benefit == "diamond" || benefit == "platinum") {
            $(".benefits div.special2 strong").text("Access to Purchase half strip");
        } else if (benefit == "gold" || benefit == "silver") {
            $(".benefits div.special2 strong").text("Access to Presale");
        } else {
            $(".benefits div.special2 strong").html("&nbsp;");
        }
        
        $(".benefits div").removeClass('active');
        var benefits = $(".benefits div." + benefit);

        benefits.addClass('active');
    }
    return false;
}

function toggleSubsections() {
    var sub = $(this).attr("data-id");
    var parent = $(this).closest("section");
    if (!$(this).hasClass('active')) {
        $(this).addClass('active');
        $(this).siblings("a").removeClass('active');
        
        parent.find(".subsection-block").removeClass('active');
        parent.find(".subsection-block[data-id=" + sub +"]").addClass('active');
    }
    return false;
}

function toggleExperiencePackages() {
    var sub = $(this).find("option:selected").val();
    if (sub != ""){
        $("#fan-experience-packages .package,#fan-experience-packages .gallery").removeClass('active');
        $("#fan-experience-packages .package[data-id='" + sub +"'],#fan-experience-packages .gallery[data-id='" + sub +"']").addClass('active');
    }
}

function toggleThemes() {
    var sub = $(this).find("option:selected").val();
    if (sub != ""){
        $("#theme-nights .package,#theme-nights .gallery").removeClass('active');
        $("#theme-nights .package[data-id='" + sub +"'],#theme-nights .gallery[data-id='" + sub +"']").addClass('active');
    }
}

function toggleFutureStars() {
    var sub = $(this).find("option:selected").val();
    if (sub != ""){
        $("#future-stars .subsection-block").removeClass('active');
        $("#future-stars .subsection-block[data-id='" + sub +"']").addClass('active');
    }
}

function toggleFaqs() {
    var faq = $(this).attr("data-id");
    if (!$(this).hasClass('active')) {
        $(this).addClass('active');
        $(this).siblings("div").removeClass('active');
        
        $(".answers div").removeClass('active');
        $(".answers div[data-id=" + faq +"]").addClass('active');
    }
}

function openGroupGallery() {
    $(".gallery-links a").first().click();
    return false;
}

function openMeetingGallery() {
    var gallery = $(this).attr("data-id");
    $("[data-fancybox='" + gallery +"']").first().click();
    return false;
}

function toggleGalleryInfo() {
    if (!$(this).hasClass('active')){
        $(this).addClass('active');
        $(this).siblings("a").removeClass('active');
        $(this).parent().prev().addClass('active');
    }
    return false;
}

function closeGalleryInfo() {
    if (!$(this).hasClass('active')){
        $(this).addClass('active');
        $(this).siblings("a").removeClass('active');
        $(this).parent().prev().removeClass('active');
    }
    return false;
}


function videoFullScreen(){
    var winWidth = $(window).width();
    if(winWidth/$('.videoContainer').outerHeight() > 16/9){
        $('.videoContainer video').css({
            'width': '100%',
            'height': 'auto',
        });
    } else {
        $('.videoContainer video').css({
            'width': 'auto', 
            'height': '100%',
        });
    }
    // if(winWidth <= 767){
    //     videoControl('pause');
    // }
}

function videoControlClick() {
    $("html").removeClass('video-playing');
    var video = $('#video-background')[0];
    if (typeof video !== 'undefined') {
        video.muted = true;
        video.play();
    }
}

function initMainWindowOnLoad(){
    doVideo();
}

function doVideo(){
    var video = $('#video-background')[0];
    if(typeof video !== 'undefined'){
        video.muted = true;
        video.play();
    }
}

// Video Modal
function showFullVideo() {
    $('body').addClass('show-video');
    $('.video, .close-video-holder').on('click', onCloseVideo);
    $('.video-holder, .video-content').on('click', onPrevent);
    // $('.video-content').html('<iframe id="vimeo_player" src="https://player.vimeo.com/video/' + videoID + '?api=1&autoplay=1&player_id=vimeo_player" width="100%" height="100%" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>');   
    $('.video-content').html('<video loop="loop" id="video-section" preload="auto" autoplay loop playsinline><source src="https://player.vimeo.com/external/305516749.hd.mp4?s=2356c702cfb19ed41e1b69e7170691ccb173fbb4&profile_id=175" type="video/mp4">" Your browser does not support the video tag. "</video>');
    // var iframe = $('#vimeo_player')[0];
    // var player = $f(iframe);
    // player.addEvent('ready', function() {
    //     player.addEvent('finish', onCloseVideo);
    // });
    var videoWrapper = $('.video-content');
    var video = videoWrapper.find('video');
    videoWrapper.addClass('active');
    video.get(0).currentTime = 0;
    video[0].play();
    video[0].muted = false;
    video.prop('loop', false);
}

function onCloseVideo() {
    $('body').removeClass('show-video');
    $('.video, .close-video-holder').off('click', onCloseVideo);
    $('.video-holder').off('click', onPrevent);
    setTimeout(function() { $('.video-content').html(''); }, 1000);
}

function onPrevent(e) {
    e.stopPropagation();
}

function closeModal() {
    $("html").removeClass('open-modal');
    return false;
}

function openModal() {
    $("html").addClass('open-modal');
    return false;
}

function closeAccordion() {
    $(this).parents().eq(1).prev().click();
    return false;
}

// Function to ease scroll on menu anchor clicks 
function anchorScroll(this_obj, that_obj, base_speed) {
    var this_offset = this_obj.offset();
    var that_offset = that_obj.offset();
    var offset_diff = Math.abs(that_offset.top - this_offset.top);

    var speed = (offset_diff * base_speed) / 1000;

    if (speed > 2000) {
        speed = 2000;
    }

    $("html,body").animate({
        scrollTop: that_offset.top - offsetScroll
    }, {
        duration: speed,
        easing: "easeInOutSine"
    }).promise().then(function() {
        $("body").removeClass('linking');
    });
}
