<?php

	header('Content-Type: application/json');	
	//username and password from form change these to what was submitted
	// $username = 'deuce@clintdempsey.com';
	// $password = 'Goals2016';
	$username = $_POST['email'];
	$password = $_POST['password'];
	


	//Generate JWT Token
	$secret = "Channel1Renewal2017";
	$issuer = "Seattle Sounders FC 2017";
	$data = "{\"username\":\"" . $username . "\"}";
	$algorithm = 'HS256';
	$date = new DateTime();
	$date->modify('+900 seconds');
	$expiry = $date->getTimestamp() * 1000;	
	$header = array(
	    "alg" => $algorithm,
	    "type" => "JWT",
	);
	$payload = array(
	    "iss" => $issuer,
	    "data" => $data,
	    "exp" => $expiry,
	);

	$unsignedToken = urlsafeB64Encode(json_encode($header)) . "." . urlsafeB64Encode(json_encode($payload));
	$signatureHash = hash_hmac('SHA256', $unsignedToken, $secret, true);
	$signature = urlsafeB64Encode($signatureHash);
	$token = $unsignedToken . '.' . $signature;


	//Create post array to submit to endpoint

	$formData = array();
	$formData['username'] = $username;
	$formData['password'] = $password;
	$formData['token'] = $token;
	
	$postData = http_build_query($formData);
	$postOptions = array('http' =>
		array(
			'method'  => 'POST',
			'header'  => 'Content-type: multipart/form-data',
			'content' => $postData
		)
	);

	$url = 'https://api.soundersapp.com/1.0/partner/user/login';
	$streamContext = stream_context_create($postOptions);
	$streamResult = file_get_contents($url, false, $streamContext);
	$jsonResult = json_decode($streamResult);	
	
	//Commented out lines were for testing purposes to make sure I was generating variables correctly.
	$returnObject = array();
	//$returnObject['expiry'] = $expiry;
	//$returnObject['form-data'] = $formData;
	//$returnObject['header-data'] = $header;
	//$returnObject['payload-data'] = $payload;
	//$returnObject['unsigned-token'] = $unsignedToken;
	//$returnObject['signature-hash'] = $signatureHash;
	//$returnObject['token'] = $token;
	//$returnObject['json'] = $jsonResult;
	$returnObject['result'] =  $jsonResult->{'result'};
	$returnObject['accountNumber'] =  $jsonResult->{'accountNumber'};
	$returnObject['username'] =  $jsonResult->{'username'};
	echo json_encode($returnObject);

	function base64Object($input) {
		//$inputWords = utf8_encode(json_encode($input));
		$inputWords = json_encode($input);
		$base64 = urlsafeB64Encode($inputWords);
		$output = $base64; //removeIllegalCharacters($base64);
		return $output;
	}
	function urlsafeB64Encode($input) {
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }
?>