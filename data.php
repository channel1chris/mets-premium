<?php

session_start();
date_default_timezone_set ("UTC");

include_once "report/includes/classes/Database.php";
include_once "report/includes/classes/Browser.php";
include_once "report/includes/models/Tracking.php";
include_once "report/includes/models/Accounts.php";

$purl =  isset($_GET['purl']) ? $_GET['purl']  : "";
$isRealPurl = false;
$account = array();
$myaccount = array();
$firstname = $lastname = $company = "";
$firstname = "Hawks Fan";
$rep = "Your Services Team";
$repPhone = "111-111-1111";
$repEmail = "tickets@hawks.com";
$fullpayment = 0;
$fullname = "";
$account_num = "";
$special = false;
$location = "";
$autorenewal = false;
$rofr = false;
$loadedticket = false;
$loadedticket2 = false;
$loadedprice = 0;
$parking = false;
$parkingvalue = 0;
$renewDate = "March 15";

$tableAccount = "hawks2018_accounts";
$tableTracking = "hawks2018_tracking";

function checkStrUrl($s) {
    $str = "ico,js,txt,html,png,jpg,jpeg,gif,php,css,wotf,css";
    $a = explode(",", $str);
    $r = true;
    for ($i=0; $i<count($a); $i++) {
        $es = "." . $a[$i];
        if (strlen($s)>strlen($es)) {
            if (substr($s, strlen($s)-strlen($es)) == $es) {
                $r = false;
                break;
            }
        }
    }
    return $r;
}

setlocale(LC_MONETARY, 'en_US');
function getNumberValue($s) {
	$s = str_replace("$", "", $s);
	$s = str_replace(",", "", $s);
	//$s = str_replace("-", "", $s);
	$s = str_replace(" ", "", $s);
	if (substr($s, 0, 1) == "(" && substr($s, strlen($s)-1, 1) == ")") {
		$s = "-" . substr($s, 1, strlen($s)-2);
	}
	//echo $s;
	if ($s == "" || $s == "-" || $s == "-0" || $s == "-0.00" ) {
		return 0;
	} else {
		return (float)$s;
	}
}
function getIntString($n) {
	$s = "" . getNumberValue($n);
	$a = explode(".", $s);
	return $a[0];
}
function getMoneyString($n) {
	return "$" . number_format(getNumberValue($n), 2, ".", "," );
}
function getMoneyStringInt($n) {
	$s = "$" . number_format(getNumberValue($n), 0, ".", "," );
	return $s;
}
function getStringInt($n) {
	$s = number_format(getNumberValue($n), 0, ".", "," );
	return $s;
}

if(checkStrUrl($purl)) {
    $pdo = Database::getInstance()->getConnection();

    if($purl != "") {
        try {
            $q = "SELECT acc.* FROM ".$tableAccount." as acc WHERE acc.purl = :purl";

            $stmt = $pdo->prepare($q);
            $stmt->execute(array(
                ":purl" => $purl
            ));

            if($stmt->rowCount() > 0) {
                $isRealPurl = true;
                $rowData = $stmt->fetchAll(PDO::FETCH_ASSOC);

				$firstname = $rowData[0]['FirstName'];
				$lastname = $rowData[0]['LastName'];
				$fullname = $rowData[0]['FullName'];
				$rep = $rowData[0]['Rep'];
				$repPhone = $rowData[0]['RepPhone'];
				$repEmail = $rowData[0]['RepEmail'];
				$account_num = $rowData[0]['Account'];
				$id = $rowData[0]["id"];
				$location = strtolower(str_replace(" ", "-", $rowData[0]["Location"]));
                if ($rowData[0]["AutoRenew"] == "Y"){
                    $autorenewal = true;
                    if ($rowData[0]['AutoRenewDate'] == "First"){
                        $renewDate = "March 1";
                    }
                }
                if ($rowData[0]["ROFR"] == "Y"){
                    $rofr = true;
                }
                if ($rowData[0]["BenefitsFb"] > 79){
                    $loadedticket = true;
                } elseif ($rowData[0]["BenefitsFb"] > 0){
                    $loadedticket2 = true;
                    $loadedprice = $rowData[0]["BenefitsFb"];
                }
                if ($rowData[0]["ParkingValue"] > 0) {
                    $parking = true;
                    $parkingvalue = $rowData[0]["ParkingValue"];
                }
                $account['Account'] = $rowData[0]['Account'];
                $myaccount[] = $rowData;

                $_SESSION['myaccount'] = $rowData;
				$_SESSION['firstname'] = $firstname;
				$_SESSION['lastname'] = $lastname;
                $_SESSION['fullname'] = $fullname;
				$_SESSION['account'] = $account;
				$_SESSION['purl'] = $purl;
				$_SESSION['rep'] = $rep;
                $_SESSION['repEmail'] = $repEmail;
                $_SESSION['date'] = $renewDate;

                $account = new Accounts($pdo);
                $account->saveHit($_SESSION['account']['Account']);
            }

            $tracking = new Tracking($pdo);
            $tracking->updateTrackingParams("hits");
            $tracking->saveBrowsingDevice();
        } catch(PDOException $e) {

        }
    } else {
        $_SESSION['account'] = $account;
        session_destroy();

        $tracking = new Tracking($pdo);
        $tracking->updateTrackingParams("hits");
        $tracking->saveBrowsingDevice();
    }

    include_once("renewal.php");

}

exit;


